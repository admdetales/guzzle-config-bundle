<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\GuzzleConfigBundle\Tests\Functional;

use Nfq\Bundle\GuzzleConfigBundle\Manager\BaseApiManagerInterface;
use Nfq\Bundle\GuzzleConfigBundle\Tests\app\Model\Animal;
use Nfq\Bundle\GuzzleConfigBundle\Tests\app\Model\Zoo;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ApiTest extends KernelTestCase
{
    /**
     * @var BaseApiManagerInterface
     */
    private $manager;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        parent::setUp();
        self::bootKernel();
        $this->manager = self::$kernel->getContainer()->get('nfq_guzzle_config.client.foo.manager');
    }

    /**
     * @test
     */
    public function testGetZoo()
    {
        $output = $this->manager->call('getZoo');
        $this->assertInstanceOf(Zoo::class, $output);
        /** @var $output Zoo */
        $this->assertSame('Kaunas ZOO', $output->getTitle());
        $this->assertSame(10, $output->getTotalAnimals());
        $this->assertTrue($output->isFull());
        $this->assertCount(1, $output->getAnimals());
        $animal = $output->getAnimals()[0];
        $this->assertInstanceOf(Animal::class, $animal);
        $this->assertSame('cat', $animal->getType());
        $this->assertSame(4, $animal->getAge());
        $this->assertSame('male', $animal->getGender());
        $this->assertSame('Barkley', $animal->getName());
    }
}
