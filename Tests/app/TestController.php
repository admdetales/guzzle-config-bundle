<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\GuzzleConfigBundle\Tests\app;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class TestController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function getZooAction()
    {
        return new JsonResponse(
            [
                'title' => 'Kaunas ZOO',
                'totalAnimals' => 10,
                'isFull' => true,
                'animals' => [
                    [
                        'type' => 'cat',
                        'name' => 'Barkley',
                        'gender' => 'male',
                        'age' => 4,
                    ],
                ],
            ]
        );
    }
}
