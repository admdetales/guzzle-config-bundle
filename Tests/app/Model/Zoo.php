<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\GuzzleConfigBundle\Tests\app\Model;

use JMS\Serializer\Annotation as Serializer;

class Zoo
{
    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\SerializedName("title")
     */
    private $title;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\SerializedName("totalAnimals")
     */
    private $totalAnimals;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     * @Serializer\SerializedName("isFull")
     */
    private $isFull;

    /**
     * @var Animal[]
     * @Serializer\Type("array<Nfq\Bundle\GuzzleConfigBundle\Tests\app\Model\Animal>")
     * @Serializer\SerializedName("animals")
     */
    private $animals;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getTotalAnimals(): int
    {
        return $this->totalAnimals;
    }

    /**
     * @return bool
     */
    public function isFull(): bool
    {
        return $this->isFull;
    }

    /**
     * @return Animal[]
     */
    public function getAnimals(): array
    {
        return $this->animals;
    }
}
