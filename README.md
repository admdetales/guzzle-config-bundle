NFQ Guzzle Config Bundle
========================

Guzzle Config Bundle is a library that wraps [Guzzle services](https://github.com/guzzle/guzzle-services), 
provides simplier api configuration, more powerful response parsing (custom response transformers)
and easy-to-add middlewares.

Documentation
-------------

* [Installation](./Resources/doc/install.md)
* [Usage](./Resources/doc/configuration_and_usage.md)
* [Usage with JMS response transformer](./Resources/doc/configuration_and_usage_jms.md)
* [Base api manager](./Resources/doc/base_api_manager.md)
* [Middleware](./Resources/doc/middleware.md)
* [FAQ](./Resources/doc/faq.md)
