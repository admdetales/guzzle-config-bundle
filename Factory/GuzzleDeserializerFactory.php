<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\GuzzleConfigBundle\Factory;

use GuzzleHttp\Command\Guzzle\DescriptionInterface;
use GuzzleHttp\Command\Guzzle\Deserializer;

class GuzzleDeserializerFactory
{
    /**
     * @param DescriptionInterface $description
     * @param bool $process
     * @param array $responseLocations
     * @return Deserializer
     */
    public function create(
        DescriptionInterface $description,
        bool $process = true,
        array $responseLocations = []
    ): Deserializer {
        return new Deserializer($description, $process, $responseLocations);
    }
}
