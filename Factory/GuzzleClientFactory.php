<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\GuzzleConfigBundle\Factory;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Command\Guzzle\Description;
use Nfq\Bundle\GuzzleConfigBundle\Client\GuzzleClient;
use Nfq\Bundle\GuzzleConfigBundle\Transformer\ResponseTransformersChainInterface;

class GuzzleClientFactory
{
    /**
     * @var ResponseTransformersChainInterface
     */
    protected $responseTransformersChain;

    /**
     * @param ResponseTransformersChainInterface $responseTransformersChain
     */
    public function __construct(ResponseTransformersChainInterface $responseTransformersChain)
    {
        $this->responseTransformersChain = $responseTransformersChain;
    }

    /**
     * @param ClientInterface $client
     * @return GuzzleClient
     */
    public function getClient(ClientInterface $client): GuzzleClient
    {
        return new GuzzleClient(
            $client,
            new Description($client->getConfig()),
            $this->responseTransformersChain,
            []
        );
    }
}
