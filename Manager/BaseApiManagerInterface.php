<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\GuzzleConfigBundle\Manager;

use GuzzleHttp\Command\CommandInterface;
use GuzzleHttp\Command\ResultInterface;

interface BaseApiManagerInterface
{
    /**
     * @param string $commandName
     * @param array $arguments
     * @return ResultInterface|mixed
     */
    public function call(string $commandName, array $arguments = []);

    /**
     * @param string $commandName
     * @param array $arguments
     * @return CommandInterface
     */
    public function getCommand(string $commandName, array $arguments = []): CommandInterface;

    /**
     * @param CommandInterface $command
     * @return mixed
     */
    public function getResponse(CommandInterface $command);
}
