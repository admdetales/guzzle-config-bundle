Configuration and Usage (JMS Serializer)
========================================

Define your client
------------------

The tag line is important, and requires both the `name: nfq_guzzle_config.client` and `alias` parts.

```yaml
# services.yml

services:
    app.client.foo:
        class: GuzzleHttp\Client
        tags:
            - { name: nfq_guzzle_config.client, alias: foo }
        arguments:
            -
                baseUrl: "http://foo"
                headers:
                    Content-Type: 'application/json'
                    Accept: 'application/json'
                timeout: 20
                connect_timeout: 20
                # Base transformer class which will apply for all operations
                transformerClass: Nfq\Bundle\GuzzleConfigBundle\Transformer\JmsResponseTransformer
                operations:
                    readBar:
                        httpMethod: "GET"
                        uri: "/bar/{barId}"
                        # The model used to deserialize the response into
                        responseModel: AppBundle\Model\Bar
                        # Define operations transformer class (this has priority over base transformer class)
                        data:
                            transformerClass: GuzzleHttp\Command\Guzzle\Deserializer
                            transformResponse: true # if not defined, value equal to true
                        parameters:
                            barId:
                                type: "string"
                                location: "uri"
                                required: true
                            title:
                                type: "string"
                                location: "json"
                                required: false
                    # other operations here    
      
```
[Guzzle services documentation](http://guzzle3.readthedocs.io/webservice-client/guzzle-service-descriptions.html)

Use the client
--------------

A new client service will appear, called `nfq_guzzle_config.client.{the alias you used in service tag}`.
You can call the operations directly:

```php
   // @var AppBundle\Model\Bar $response
   $response = $this->get('nfq_guzzle_config.client.foo')->readBar(['barId' => 1, 'title' => 'baz']);
```

Deserialization context
-----------------------

If you wish to customize the deserialization context of the serializer,
you can do so by usage of the `data` property of the Operation.
Groups, version, and max depth checks are configurable for deserialization:

```yaml
operations:
    readBar:
        httpMethod: "GET"
        uri: "/bar/{barId}"
        responseClass: AppBundle\Model\Bar
        data:
            jms_serializer.groups: ["create", "update"]
            jms_serializer.version: 1
            jms_serializer.max_depth_checks: true
        parameters:
            barId:
                type: "string"
                location: "uri"
                required: true

```

---

Back to: [Usage](configuration_and_usage.md)

Next section: [Base api manager](base_api_manager.md)
