Installation
============

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```bash
composer require nfq/guzzle-config-bundle
```

Optionally install and enable [JMS Serializer bundle](https://packagist.org/packages/jms/serializer-bundle)
if you want to use `JmsResponseTransformer` and deserialize response to objects.

### Symfony up to 4.0

```php
<?php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // ...
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new Nfq\Bundle\GuzzleConfigBundle\NfqGuzzleConfigBundle(),
        );

        // ...
    }

    // ...
}
```

### Symfony 4.0 and up

```php
<?php
// config/bundles.php

return [
    // ...
    Nfq\Bundle\GuzzleConfigBundle\NfqGuzzleConfigBundle::class => ['all' => true],
];
```

---

Next section: [Usage](configuration_and_usage.md)
