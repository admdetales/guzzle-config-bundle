<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\GuzzleConfigBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class NfqGuzzleConfigExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        $this->processLoggerConfiguration($config['logger'], $container);
        $this->processRetryConfiguration($config['retry'], $container);
    }

    /**
     * @param array $config
     * @param ContainerBuilder $container
     */
    private function processLoggerConfiguration(array $config, ContainerBuilder $container)
    {
        $loggerService = 'nfq_guzzle_config_bundle.middleware.logger';
        $loggerServiceFormatter = 'nfq_guzzle_config_bundle.middleware.logger_message_formatter';

        if (!$config['enabled']) {
            $container->removeDefinition($loggerService);
            $container->removeDefinition($loggerServiceFormatter);

            return;
        }

        $loggerDefinition = $container->getDefinition($loggerService);

        if ($config['service']) {
            $loggerDefinition->replaceArgument(0, new Reference($config['service']));
        }

        if ($config['format']) {
            $formatterDefinition = $container->getDefinition($loggerServiceFormatter);
            $formatterDefinition->replaceArgument(0, $config['format']);
        }

        if ($config['level']) {
            $loggerDefinition->replaceArgument(2, $config['level']);
        }
    }

    /**
     * @param array $config
     * @param ContainerBuilder $container
     */
    private function processRetryConfiguration(array $config, ContainerBuilder $container)
    {
        $retryService = 'nfq_guzzle_config_bundle.middleware.retry';

        if (!$config['enabled']) {
            $container->removeDefinition($retryService);

            return;
        }

        $retryDefinition = $container->getDefinition($retryService);
        $retryDefinition->replaceArgument(0, $config['max_retries']);
        $retryDefinition->replaceArgument(1, $config['delay']);
        $retryDefinition->replaceArgument(2, new Reference($config['logger']));
        $retryDefinition->replaceArgument(3, $config['log_level']);
    }
}
