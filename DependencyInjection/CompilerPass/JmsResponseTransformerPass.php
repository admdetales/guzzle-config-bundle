<?php

namespace Nfq\Bundle\GuzzleConfigBundle\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;

class JmsResponseTransformerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if ($container->has('jms_serializer')) {
            $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../../Resources/config'));
            $loader->load('jms_response_transformer.yml');
        }
    }
}
