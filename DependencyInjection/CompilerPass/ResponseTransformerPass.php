<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\GuzzleConfigBundle\DependencyInjection\CompilerPass;

use Nfq\Bundle\GuzzleConfigBundle\Transformer\ResponseTransformersChain;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ResponseTransformerPass implements CompilerPassInterface
{
    public const SERVICE_TAG = 'nfq_guzzle_config.response_transformer';

    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has(ResponseTransformersChain::class)) {
            return;
        }

        $definition = $container->findDefinition(ResponseTransformersChain::class);
        $taggedServices = $container->findTaggedServiceIds(self::SERVICE_TAG, true);

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addResponseTransformer', [new Reference($id)]);
        }
    }
}
